import json
import os
import requests


ENDPOINT = "http://127.0.0.1:8000/api/status/"

image_path = os.path.join(os.getcwd(),"man.png")

def do_img(method='get', data={}, is_json=True, img_path=None):
    headers = {}
    if is_json:
        headers['content-type'] = "application/json"
        data = json.dumps(data)

    if img_path is not None:
        with open(img_path, 'rb') as image:
            file_data={
                'image':image,
            }
            r = requests.request(method, ENDPOINT, data=data, headers=headers, files=file_data)
    else:
        r = requests.request(method, ENDPOINT , data=data, headers = headers)

    print(r.text)
    print(r.status_code)
    return r


def do(method='get', data={}, is_json=True):
    headers = {}
    if is_json:
        headers['content-type'] = "application/json"
        data = json.dumps(data)

    r = requests.request(method, ENDPOINT , data=data, headers = headers)
    print(r.text)
    print(r.status_code)
    return r

if __name__ == "__main__":
    # do(data={'id':55})
    # do(method='delete', data={'id':14})
    # do(method='put', data={'id':12, "content":"Some Cool Neww Content@!", "user":1})
    # do(method='post', data={"content":"Some Cool Neww Content@!", "user":1})
    # do_img(method='post', data={'user':1, 'content':""}, img_path=image_path, is_json=False)
    do_img(method='put', data={"id":13,'user':1, 'content':"Update the content with image!"},
           img_path=image_path, is_json=False)
