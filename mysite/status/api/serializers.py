from rest_framework import serializers
from status.models import Status


'''
Serializers ==> JSON
Serializers ==> Validate Data
'''

# class CustomSerializer(serializers.Serializer):
#     content = serializers.CharField()
#     email = serializers.EmailField()

class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ['pk','user','content','image']

    # def validate_content(self, value):
    #     if len(value) > 1000000:
    #         raise serializers.ValidationError('This is way too long')
    #     return value

    def validate(self, data):
        content = data.get('content', None)
        image = data.get('image', None)

        if content == '':
            content = None

        if content is None and image is None:
            raise serializers.ValidationError('Content or image is required')
        return data
