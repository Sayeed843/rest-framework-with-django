from django.urls import path
from status.api.views import (StatusAPIView,StatusAPIDetailView)
"""
 StatusCreateAPIView, StatusDetailAPIView, StatusUpdateAPIView, StatusDeleteAPIView
"""

urlpatterns = [
    path('',StatusAPIView.as_view()),
    path('<id>/', StatusAPIDetailView.as_view()),
    # path('create/',StatusCreateAPIView.as_view()),
    # path('<id>/', StatusDetailAPIView.as_view()),
    # path('<id>/update/', StatusUpdateAPIView.as_view()),
    # path('<pk>/delete/', StatusDeleteAPIView.as_view()),
]



#Start with,
"""
/api/status/ -->  List
/api/status/create --> Create
/api/status/12/  --> Detail
/api/status/12/update/ --> update
/api/status/12/delete/ -->  Delete
"""


#End with
"""
/api/status/ --> List --> CRUD
/api/status/1/ --> Detail --> CRUD
"""


#Final
# /api/status --> CRUD & LS
